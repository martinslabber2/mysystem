#!/bin/bash
set -e  # Be strict, if any step fails bailout.

# For now this has to be cloned into $HOME
MYSYSTEM="$HOME/mysystem"
DOTVIM_SRC="$MYSYSTEM/vim"
DOTVIM="$HOME/.vim"
VIMRC_SRC="$MYSYSTEM/vim/vimrc"
VIMRC="$HOME/.vimrc"

echo "Configure Vim"

# Link the .vim directory.
echo "Setup VIM Directory: $DOTVIM"
if [ ! -d "$DOTVIM" ]
then
        echo "    Link dotVim"
        ln -s $DOTVIM_SRC $DOTVIM
else
        if [ "$(readlink $DOTVIM)" = "$DOTVIM_SRC" ]
        then
            echo "    dotVim was already correctly linked"
        else
            echo
            echo "dotVim is already Linked, remove $DOTVIM and run again"
            exit 1
        fi
fi

# Link the .vimrc file.
echo "Setup VIM Config: $VIMRC"
if [ ! -f "$VIMRC" ]
then
        echo "    Link VimRC"
        ln -s $VIMRC_SRC $VIMRC
else
        if [ "$(readlink $VIMRC)" = "$VIMRC_SRC" ]
        then
            echo "    VimRC was already correctly linked"
        else
            echo
            echo "VimRC is already Linked, remove $VIMRC and run again"
            exit 1
        fi
fi

# Ensure the submodules are uptodate.
cd $MYSYSTEM
git submodule init
git submodule update

# Install the VIM modules.
vim +BundleInstall +qall
